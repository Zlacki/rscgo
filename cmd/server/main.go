package main

import (
	"bitbucket.org/zlacki/rscgo/pkg/server"
)

func main() {
	server.Start()
}

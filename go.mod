module bitbucket.org/zlacki/rscgo

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/jessevdk/go-flags v1.4.0
	github.com/mattn/go-sqlite3 v1.11.0
	golang.org/x/crypto v0.0.0-20190820162420-60c769a6c586
)
